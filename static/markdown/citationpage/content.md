
## How to cite PRIDE Resources

All PRIDE Archive and PX identifiers for a dataset.

## Citing PRIDE Resources:

- **Citing the PRIDE Generally**: Vizcaíno JA, Csordas A, del-Toro N, Dianes JA, Griss J, Lavidas I, Mayer G, Perez-Riverol Y, Reisinger F, Ternent T, Xu QW, Wang R, Hermjakob H.
  [2016 update of the PRIDE database and related tools](http://nar.oxfordjournals.org/content/44/D1/D447.long). Nucleic Acids Res. 2016 Jan 1;44(D1): D447-D456. PubMed PMID:26527722.

- **Citing ProteomeXchange**: Deutsch EW. , Csordas A, Sun Z, Jarnuczak A, Perez-Riverol Y, Ternent T, Campbell D, Bernal-Llinares M, Okuda S, Kawano S, Moritz RL,1 Carver JJ., Wang M, Ishihama Y, Bandeira N, Hermjakob H and Vizcaíno JA.
  [The ProteomeXchange consortium in 2017: supporting the cultural change in proteomics public data deposition](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5210636/).  Nucleic Acids Res. 2017 Jan 4; 45(Database issue): D1100–D1106.

- **When a dataset has been deposited in PRIDE and ProteomeXchange the citation of the PX should be done in this form**:

  **PX Partial**: The mass spectrometry proteomics data have been deposited to the ProteomeXchange Consortium (http://proteomecentral.proteomexchange.org)
  via the PRIDE partner repository [1] with the dataset identifier PXD000000.

  **PX Complete**: The mass spectrometry proteomics data have been deposited to the ProteomeXchange Consortium (http://proteomecentral.proteomexchange.org)
  via the PRIDE partner repository [1] with the dataset identifier PXD000000 and DOI 10.6019/PXD000000.

  [1] Vizcaino JA, et al. 2016 update of the PRIDE database and related tools. Nucleic Acids Res. 2016 Jan 1;44(D1):D447-D456.

  See some examples: [PX Partial Submission](https://www.sciencedirect.com/science/article/pii/S0092867416315914), [PX Complete Submission](https://www.frontiersin.org/articles/10.3389/fmicb.2018.00680/full)

Citing Peptidome and PRIDE Cluster resources:

- **Citing PRIDE Peptidome**: Griss J, Perez-Riverol Y, Lewis S, Tabb DL, Dianes JA, Del-Toro N, Rurik M, Walzer MW, Kohlbacher O, Hermjakob H, Wang R, Vizcaíno JA.
  [Recognizing millions of consistently unidentified spectra across hundreds of shotgun proteomics datasets](https://www.ncbi.nlm.nih.gov/pubmed/27493588). Nat Methods. 2016 Aug;13(8):651-656. PubMed

Citing PRIDE Tools:

- **PRIDE Inspector Toolsuite**: Perez-Riverol Y, Xu QW, Wang R, Uszkoreit J, Griss J, Sanchez A, Reisinger F, Csordas A, Ternent T, del Toro N, Dianes JA, Eisenacher M, Hermjakob H, Vizcaíno JA.
 [PRIDE Inspector Toolsuite: moving towards a universal visualization tool for proteomics data standard formats and quality assessment of ProteomeXchange datasets.](http://www.mcponline.org/content/15/1/305.long). Mol Cell Proteomics 2016 Jan; 15(1):305-17. PubMed PMID: 26545397.

- **PRIDE Cluster** or **Spectra Cluster** Toolsuite: Griss J, Perez-Riverol Y, Lewis S, Tabb DL, Dianes JA, Del-Toro N, Rurik M, Walzer MW, Kohlbacher O, Hermjakob H, Wang R, Vizcaíno JA.
 [Recognizing millions of consistently unidentified spectra across hundreds of shotgun proteomics datasets](https://www.ncbi.nlm.nih.gov/pubmed/27493588). Nat Methods. 2016 Aug;13(8):651-656. PubMed


Previous manuscripts of PRIDE team can be found in [PRIDE Team GoogleScholar](https://scholar.google.co.uk/citations?user=tzHv1ZcAAAAJ&hl=en)

